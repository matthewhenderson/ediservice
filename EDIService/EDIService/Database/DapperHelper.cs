﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;

namespace EDIService.Database
{
    public class DapperHelper
    {
        private readonly string _connectionString;

        public DapperHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<T> Query<T>(string sql, object param = null)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                return conn.Query<T>(sql, param);
            }
        }

        public int Execute(string sql, object param = null)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                return conn.Execute(sql, param);
            }
        }
    }
}
