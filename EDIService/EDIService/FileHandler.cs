﻿using System;
using System.IO;

namespace EDIService
{
    /// <summary>
    /// For handling working with files
    /// </summary>
    public class FileHandler
    {
        private readonly string _writeDirectory; //the directory to write files to

        public FileHandler(string directory)
        {
            _writeDirectory = directory;
            Initialize();
        }

        /// <summary>
        /// To initialize anything the file handler needs setup
        /// </summary>
        private void Initialize()
        {
            if (!Directory.Exists(_writeDirectory))
                Directory.CreateDirectory(_writeDirectory);
        }

        /// <summary>
        /// Writes given file contents as a file in the write directory
        /// </summary>
        /// <param name="fileName">The name of the new file</param>
        /// <param name="fileContents">The contents of a file</param>
        public void WriteFile(string fileName, string fileContents)
        {
            File.WriteAllText(Path.Combine(_writeDirectory, fileName), fileContents);
        }
    }
}