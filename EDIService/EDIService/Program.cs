﻿using System;
using System.Collections.Generic;
using EDIService.X12;

namespace EDIService
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileHandler = new FileHandler(Properties.Settings.Default.WriteDirectory);
            var processers = new List<IProcesser> { new Process940(), new Process943() };

            foreach (IProcesser processer in processers)
            {
                processer.LoadData();
                processer.GenerateFiles();
                processer.Audit();
                processer.WriteFiles(fileHandler);
            }

            var g = new Generator(false);
            string r = g.Generate940(new ControlNumber());
            Console.Write(r);

            Console.Write(Environment.NewLine);
            Console.Write(Environment.NewLine);

            r = g.Generate943(new ControlNumber());
            Console.Write(r);

            Console.Write(Environment.NewLine);
        }
    }
}
