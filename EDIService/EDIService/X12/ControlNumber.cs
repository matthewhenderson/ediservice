﻿using System;

namespace EDIService.X12
{
    public class ControlNumber
    {
        public ControlNumber()
        {
            CheckNumber();
        }

        public ControlNumber(int number)
        {
            ControlNumberValue = number;
            CheckNumber();
        }

        private void CheckNumber()
        {
            if (ControlNumberValue < 1 || ControlNumberValue > 999999999)
                ControlNumberValue = 1;
        }

        public int ControlNumberValue { get; private set; }

        public override string ToString()
        {
            string value = ControlNumberValue.ToString();
            while (value.Length < 4)
            {
                value = "0" + value;
            }

            return value;
        }
    }
}
