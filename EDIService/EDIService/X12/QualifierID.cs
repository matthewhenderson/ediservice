﻿using System;

namespace EDIService.X12
{
    /// <summary>
    /// Company specific qualifier details
    /// </summary>
    public class QualifierID
    {
        public QualifierID(string qualifier, string id)
        {
            InterchangeIDQualifier = qualifier;
            InterchangeID = id;
        }

        /// <summary>
        /// The specific system that the company ID is from
        /// </summary>
        public string InterchangeIDQualifier { get; set; }

        /// <summary>
        /// The company ID
        /// </summary>
        public string InterchangeID { get; set; }
    }
}
