﻿using System;
using System.IO;
using OopFactory.X12.Parsing.Model;
using OopFactory.X12.Parsing.Model.Typed;

namespace EDIService.X12
{
    /// <summary>
    /// 
    /// </summary>
    public class Generator
    {
        private readonly bool _production;//true if not used for testing
        private const char SegmentTerminator = '\0';
        private const char ElementSeparator = '*';
        private const char SubElementSeparator = '>';

        public Generator(bool production)
        {
            _production = production;
        }

        public string Generate940(ControlNumber controlNumber)
        {
            Interchange message = BuildInterchange(controlNumber, new QualifierID("01", ""), new QualifierID("01", ""));
            FunctionGroup function = BuildFunctionGroup(message);
            Transaction trans = function.AddTransaction("940", controlNumber.ToString());

            var w05Builder = new W05SegmentBuilder(trans)
            {
                W0501_OrderStatusCode = "05",
                W0502_DepositorOrderNumber = "SA",
                W0503_PurchaseOrderNumber = "S41000439",
                W0504_LinkSequenceNumber = "20100810",
                W0505_MasterReferenceNumber = "20100810",
                W0506_TransactionTypeCode = "20100810",
                W0507_ActionCode = "20100810",
                W0508_PurchaseOrderTypeCode = "20100810"
            };
            Segment w05Seg = w05Builder.Segment;

            TypedLoopN1 billTo = trans.AddLoop(new TypedLoopN1());
            billTo.N101_EntityIdentifierCodeEnum = EntityIdentifierCode.BillToParty;
            billTo.N102_Name = "The Scheduling Coordinator, Inc";

            TypedSegmentN3 billToAddress = billTo.AddSegment(new TypedSegmentN3());
            billToAddress.N301_AddressInformation = "53241 Hamilton Dr";

            TypedSegmentN4 billToLocale = billTo.AddSegment(new TypedSegmentN4());
            billToLocale.N401_CityName = "Palo Alto";
            billToLocale.N402_StateOrProvinceCode = "CA";
            billToLocale.N403_PostalCode = "95622";
            billToLocale.N404_CountryCode = "US";

            TypedLoopN1 remitTo = trans.AddLoop(new TypedLoopN1());
            remitTo.N101_EntityIdentifierCodeEnum = EntityIdentifierCode.PartyToReceiveCommercialInvoiceRemittance;
            remitTo.N102_Name = "Bank of America- (Mkt and GMC)";

            TypedSegmentN3 remitToAddress = remitTo.AddSegment(new TypedSegmentN3());
            remitToAddress.N301_AddressInformation = "1850 Gateway Boulevard";

            TypedSegmentN4 remitToLocale = remitTo.AddSegment(new TypedSegmentN4());
            remitToLocale.N401_CityName = "Concord";
            remitToLocale.N402_StateOrProvinceCode = "CA";
            remitToLocale.N403_PostalCode = "94520";
            remitToLocale.N404_CountryCode = "US";

            TypedSegmentREF remitToRef1 = remitTo.AddSegment(new TypedSegmentREF());
            remitToRef1.REF01_ReferenceIdQualifier = "11";
            remitToRef1.REF02_ReferenceId = "1233626208";

            TypedSegmentREF remitToRef2 = remitTo.AddSegment(new TypedSegmentREF());
            remitToRef2.REF01_ReferenceIdQualifier = "01";
            remitToRef2.REF02_ReferenceId = "026009593";

            TypedSegmentITD itd = trans.AddSegment(new TypedSegmentITD());
            itd.ITD01_TermsTypeCode = "03";
            itd.ITD06_TermsNetDueDate = Convert.ToDateTime("10/20/1998");

            TypedLoopIT1 it1 = trans.AddLoop(new TypedLoopIT1());
            it1.IT101_AssignedIdentification = "1";
            it1.IT102_QuantityInvoiced = 1;
            it1.IT103_UnitOrBasisForMeasurementCode = UnitOrBasisOfMeasurementCode.Each;
            it1.IT104_UnitPrice = 2896035.3m;

            TypedLoopPID pid = it1.AddLoop(new TypedLoopPID());
            pid.PID01_ItemDescriptionType = "X";
            pid.PID05_Description = "RMR Scheduling Coordinator - Estimated RMR";

            TypedSegmentTDS tds = trans.AddSegment(new TypedSegmentTDS());
            tds.TDS01_AmountN2 = 289603530;

            TypedSegmentCTT ctt = trans.AddSegment(new TypedSegmentCTT());
            ctt.CTT01_NumberOfLineItems = 1;

            string x12 = message.SerializeToX12(true);

            return x12;
        }

        public string Generate943(ControlNumber controlNumber)
        {
            Interchange message = BuildInterchange(controlNumber, new QualifierID("01", ""), new QualifierID("01", ""));
            FunctionGroup function = BuildFunctionGroup(message);
            Transaction trans = function.AddTransaction("943", controlNumber.ToString());

            TypedSegmentBIG big = trans.AddSegment(new TypedSegmentBIG());
            big.BIG01_InvoiceDate = Convert.ToDateTime("10/14/1998");
            big.BIG02_InvoiceNumber = "3662";
            big.BIG07_TransactionTypeCode = "N6";

            TypedLoopN1 billTo = trans.AddLoop(new TypedLoopN1());
            billTo.N101_EntityIdentifierCodeEnum = EntityIdentifierCode.BillToParty;
            billTo.N102_Name = "The Scheduling Coordinator, Inc";

            TypedSegmentN3 billToAddress = billTo.AddSegment(new TypedSegmentN3());
            billToAddress.N301_AddressInformation = "53241 Hamilton Dr";

            TypedSegmentN4 billToLocale = billTo.AddSegment(new TypedSegmentN4());
            billToLocale.N401_CityName = "Palo Alto";
            billToLocale.N402_StateOrProvinceCode = "CA";
            billToLocale.N403_PostalCode = "95622";
            billToLocale.N404_CountryCode = "US";

            TypedLoopN1 remitTo = trans.AddLoop(new TypedLoopN1());
            remitTo.N101_EntityIdentifierCodeEnum = EntityIdentifierCode.PartyToReceiveCommercialInvoiceRemittance;
            remitTo.N102_Name = "Bank of America- (Mkt and GMC)";

            TypedSegmentN3 remitToAddress = remitTo.AddSegment(new TypedSegmentN3());
            remitToAddress.N301_AddressInformation = "1850 Gateway Boulevard";

            TypedSegmentN4 remitToLocale = remitTo.AddSegment(new TypedSegmentN4());
            remitToLocale.N401_CityName = "Concord";
            remitToLocale.N402_StateOrProvinceCode = "CA";
            remitToLocale.N403_PostalCode = "94520";
            remitToLocale.N404_CountryCode = "US";

            TypedSegmentREF remitToRef1 = remitTo.AddSegment(new TypedSegmentREF());
            remitToRef1.REF01_ReferenceIdQualifier = "11";
            remitToRef1.REF02_ReferenceId = "1233626208";

            TypedSegmentREF remitToRef2 = remitTo.AddSegment(new TypedSegmentREF());
            remitToRef2.REF01_ReferenceIdQualifier = "01";
            remitToRef2.REF02_ReferenceId = "026009593";

            TypedSegmentITD itd = trans.AddSegment(new TypedSegmentITD());
            itd.ITD01_TermsTypeCode = "03";
            itd.ITD06_TermsNetDueDate = Convert.ToDateTime("10/20/1998");

            TypedLoopIT1 it1 = trans.AddLoop(new TypedLoopIT1());
            it1.IT101_AssignedIdentification = "1";
            it1.IT102_QuantityInvoiced = 1;
            it1.IT103_UnitOrBasisForMeasurementCode = UnitOrBasisOfMeasurementCode.Each;
            it1.IT104_UnitPrice = 2896035.3m;

            TypedLoopPID pid = it1.AddLoop(new TypedLoopPID());
            pid.PID01_ItemDescriptionType = "X";
            pid.PID05_Description = "RMR Scheduling Coordinator - Estimated RMR";

            TypedSegmentTDS tds = trans.AddSegment(new TypedSegmentTDS());
            tds.TDS01_AmountN2 = 289603530;

            TypedSegmentCTT ctt = trans.AddSegment(new TypedSegmentCTT());
            ctt.CTT01_NumberOfLineItems = 1;

            string x12 = message.SerializeToX12(true);

            return x12;
        }

        private Interchange BuildInterchange(ControlNumber controlNumber, QualifierID sender, QualifierID receiver)
        {
            var interchange = new Interchange(DateTime.Now, controlNumber.ControlNumberValue, _production, SegmentTerminator, ElementSeparator, SubElementSeparator)
            {
                SecurityInfoQualifier = "00",
                InterchangeSenderIdQualifier = sender.InterchangeIDQualifier,
                InterchangeSenderId = sender.InterchangeID,
                InterchangeReceiverIdQualifier = receiver.InterchangeIDQualifier,
                InterchangeReceiverId = receiver.InterchangeID
            };

            return interchange;
        }

        private FunctionGroup BuildFunctionGroup(Interchange interchange)
        {
            FunctionGroup function = interchange.AddFunctionGroup("IN", Convert.ToDateTime("1/4/1999 15:32"), 1);
            function.ApplicationSendersCode = "943274043TO";
            function.ApplicationReceiversCode = "0069088189999";
            function.ResponsibleAgencyCode = "X";
            function.VersionIdentifierCode = "004010";

            return function;
        }
    }
}
