﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OopFactory.X12.Parsing.Model;

namespace EDIService.X12
{
    /// <summary>
    /// Beginning Segment for TODO:write this
    /// </summary>
    public class W05SegmentBuilder
    {
        private Transaction _transaction;
        private const string SegementString = "W05";

        public W05SegmentBuilder(Transaction transaction)
        {
            _transaction = transaction;
            _segment = _transaction.AddSegment(SegementString);
        }

        private Segment _segment;
        /// <summary>
        /// 
        /// </summary>
        public Segment Segment
        {
            get { return _segment; }
            set { _segment = value; }
        }

        /// <summary>
        /// Order Status Code
        /// </summary>
        public string W0501_OrderStatusCode
        {
            get { return _segment.GetElement(1); }
            set { _segment.SetElement(1, value); }
        }

        /// <summary>
        /// Depositor Order Number
        /// </summary>
        public string W0502_DepositorOrderNumber
        {
            get { return _segment.GetElement(2); }
            set { _segment.SetElement(2, value); }
        }

        /// <summary>
        /// Purchase Order Number
        /// </summary>
        public string W0503_PurchaseOrderNumber
        {
            get { return _segment.GetElement(3); }
            set { _segment.SetElement(3, value); }
        }

        /// <summary>
        /// Link Sequence Number
        /// </summary>
        public string W0504_LinkSequenceNumber
        {
            get { return _segment.GetElement(4); }
            set { _segment.SetElement(4, value); }
        }

        /// <summary>
        /// Master Reference (Link) Number
        /// </summary>
        public string W0505_MasterReferenceNumber
        {
            get { return _segment.GetElement(5); }
            set { _segment.SetElement(5, value); }
        }

        /// <summary>
        /// Transaction Type Code
        /// </summary>
        public string W0506_TransactionTypeCode
        {
            get { return _segment.GetElement(6); }
            set { _segment.SetElement(6, value); }
        }

        /// <summary>
        /// Action Code
        /// </summary>
        public string W0507_ActionCode
        {
            get { return _segment.GetElement(7); }
            set { _segment.SetElement(7, value); }
        }

        /// <summary>
        /// Purchase Order Type Code
        /// </summary>
        public string W0508_PurchaseOrderTypeCode
        {
            get { return _segment.GetElement(8); }
            set { _segment.SetElement(8, value); }
        }
    }
}
