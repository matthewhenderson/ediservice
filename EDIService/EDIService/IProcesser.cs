﻿using System;

namespace EDIService
{
    /// <summary>
    /// TODO: write this
    /// </summary>
    public interface IProcesser
    {
        /// <summary>
        /// TODO: write this
        /// </summary>
        void LoadData();

        /// <summary>
        /// TODO: write this
        /// </summary>
        void GenerateFiles();

        /// <summary>
        /// TODO: write this
        /// </summary>
        void Audit();

        /// <summary>
        /// TODO: write this
        /// </summary>
        /// <param name="handler"></param>
        void WriteFiles(FileHandler handler);
    }
}
